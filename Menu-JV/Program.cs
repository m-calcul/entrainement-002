﻿
using System.ComponentModel.Design;
void affichage_menu()
{
    Console.WriteLine("MENU");
    Console.WriteLine("  Tape 1 Pour Une Nouvelle Partie");
    Console.WriteLine("  Tape 2 Pour Charger une Partie");
    Console.WriteLine("  Tape 3 Quitter");
    Console.WriteLine();
    var menuValide = false;
    do
    {
        Console.WriteLine("Quel est ton choix?");
        var menu = Console.ReadLine();
        switch (menu)
        {
            case "1":
                Console.WriteLine("Quel est ton prenom J1 ?");
                var prenom_J1 = Console.ReadLine();
                Console.WriteLine();


                Console.WriteLine();
                bool dateValide = false;
                
                do
                {
                    Console.WriteLine("Quelle est ta date de naissance ?");
                    var date_naissance = Console.ReadLine();
                    Console.WriteLine();


                    if (DateTime.TryParse(date_naissance, out var maVraiDate))
                    {
                        var comparaisonDates = DateTime.Now - maVraiDate;
                        int nbAnnees = (int)(comparaisonDates.TotalDays / 365);
                        bool ageValide = false;
                        do
                        {

                            Console.WriteLine("Quelle âge as-tu ?");
                            var age = Console.ReadLine();
                            Console.WriteLine();
                            if (int.TryParse(age, out var vraiAge))
                            {
                                if (vraiAge < 13 || vraiAge != nbAnnees)
                                {
                                    ageValide = false;
                                    Console.WriteLine("Age Invalide");
                                }
                                else
                                {
                                    ageValide = true;
                                    Console.WriteLine("Age Valide");
                                }
                            }
                            else
                            {
                                ageValide = false;
                                Console.WriteLine("Age Invalide");
                            }
                        } while (ageValide != true);

                        Console.WriteLine($"Ton prénom est bien ? {prenom_J1}, et tu es âgé de {nbAnnees} années ?");
                        Console.WriteLine();

                        dateValide = true;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("Erreur de saisie de date, ré-essaie stp !");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    Console.WriteLine();
                }
                while (!dateValide);
                break;

            case "2":
                Console.WriteLine("Charger une partie");
                Console.WriteLine();
                break;

            case "3":
                Console.WriteLine("Quitter");
                Console.WriteLine();
                Console.ReadLine();
                menuValide = true;
                break;

            default:
                Console.WriteLine("Saisie Incorrecte");
                break;
        }

    } while (menuValide != true);

}

affichage_menu();