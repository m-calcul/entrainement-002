﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetourSurLesEnums
{
    ///summary
    /// Liste des types de Forces
    ///</summary>
    public enum TypeForce
    {
        None,
        CoteObscur,
        CoteLumineux
    }
}
